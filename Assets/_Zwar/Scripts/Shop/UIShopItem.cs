using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIShopItem : MonoBehaviour
{
    Action<ShopItem> OnclickInfoCallBack;
    private ShopItem _data;
    [SerializeField]private TextMeshProUGUI _txtName;
    [SerializeField] private Image _icon;

    public void InitData(ShopItem data,Action<ShopItem> callBack)
    {
        _data = data;
        OnclickInfoCallBack = callBack;
        setDataToItem();
    }
    private void setDataToItem()
    {
        if (_data != null)
        {
            _txtName.text = _data.Name;
            if(_data.Icon!=null)
                _icon.sprite = _data.Icon;
        }
    }
    public void OnlickInfo()
    {
        OnclickInfoCallBack?.Invoke(_data);
    }
}
