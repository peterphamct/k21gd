using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PausePopup : UIPopup
{
    public GameObject PauseMenuCanvas;
    public void Pause()
    {
        PauseMenuCanvas.SetActive(true);
        Time.timeScale = 0;
    }
    public void Continue()
    {
        PauseMenuCanvas.SetActive(false);
        Time.timeScale = 1;
    }
    public void ReStart()
    {
        SceneManager.LoadScene("WorldMap");
        Time.timeScale = 1;
    }
}
