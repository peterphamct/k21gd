﻿using PlayFab;
using PlayFab.ClientModels;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Newtonsoft.Json;

public class PlayerData
{
    public string name;
    public int level;
    public float exp;

}

public class LoginPopup : UIPopup
{
    protected override void OnShown()
    {
        
    }
    public void OnclickLogin()
    {
        LoginPlayfab();
    }
   private void LoginPlayfab()
    {
        string cusID= PlayerPrefs.GetString("CustomID");
        if (string.IsNullOrEmpty(cusID))
        {
            Guid newGuid = Guid.NewGuid();
            cusID = newGuid.ToString("N");
            PlayerPrefs.SetString("CustomID", cusID);
        }
        PlayfabManager.Instance.LoginWithCustomID(cusID, LoginCallBack);
    }
    private void LoginCallBack(bool status, PlayFabError error,LoginResult result)
    {
        if (status)
        {
            Debug.Log("LoginSuccess");
            
            if (result.NewlyCreated)
            {
                InitFirstData();
            }
            else
            {
                UIManager.Instance.HidePopup(UIPoupName.Login);
                GetPlayfabData();
                //SceneManager.LoadScene("WorldMap");
            }
          
        }
        else
            Debug.LogError(error.ErrorMessage);
    }
    private void GetPlayfabData()
    {
        PlayfabManager.Instance.GetTitleDataFromPlayFab((titleData) => {
            foreach (var kvp in titleData.Data)
            {
                string key = kvp.Key;
                string value = kvp.Value.Value;
                Debug.Log($"Title Data - Key: {key}, Value: {value}");
            }
        });
    }
    private void InitFirstData()
    {
        PlayerData data = new PlayerData();
        data.level = 0;
        data.name = "name";
        data.exp = 114;
        string json = JsonConvert.SerializeObject(data);
        PlayfabManager.Instance.UpdatePlayerTitleData("PlayerData",json);
    }
}
