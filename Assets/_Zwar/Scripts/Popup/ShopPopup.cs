﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopPopup : UIPopup
{
    public string shopItemPath; // Tên thư mục trong Resources chứa ScriptableObject
    public List<ShopItem> _listDataItem; // Danh sách để lưu trữ các ScriptableObject đã tải
    public GameObject UIShopItemPrefab;
    public Transform ItemSpawnTrans;
    private void Start()
    {
        LoadScriptableObjects();
    }
    private void LoadScriptableObjects()
    {
        _listDataItem.Clear(); // Xóa danh sách hiện có (nếu có)

        // Tải tất cả các ScriptableObject trong thư mục Resources/resourceFolderPath
        ShopItem[] scriptableObjects = Resources.LoadAll<ShopItem>(shopItemPath);

        foreach (ShopItem scriptableObject in scriptableObjects)
        {
            _listDataItem.Add(scriptableObject);
        }

        if (_listDataItem.Count > 0)
        {
            foreach(var item in _listDataItem)
            {
                var itemSpawned = Instantiate(UIShopItemPrefab, ItemSpawnTrans).GetComponent<UIShopItem>();
                itemSpawned.InitData(item, OnclickInfo);
                itemSpawned.gameObject.SetActive(true);
            }
        }
    }
    private void OnclickInfo(ShopItem data)
    {
        PreviewSystemManager.Instance.ShowItem(data.Prefab);
    }
    protected override void OnShown()
    {

    }
}
