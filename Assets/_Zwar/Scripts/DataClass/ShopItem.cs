using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewShopItem", menuName = "Shop/Shop Data")]
public class ShopItem : ScriptableObject
{
    public string ID;
    public string Name;
    public int Price;
    public Sprite Icon;
    public GameObject Prefab;
}
