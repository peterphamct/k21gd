using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class EnemyMove : MonoBehaviour
{
    private Transform playerTransform;
    public NavMeshAgent navMesh;
    public float attackRange = 2.0f;

    private Animator anim;

    private float setTime = 5f;
    private float timeSinceLastUpdate = 0f;

    public int maxHealth = 100;
    public int currentHealth = 0;

    public Slider healthBar;

    private void Awake()
    {
        navMesh = GetComponent<NavMeshAgent>();
        anim = GetComponent<Animator>();
        currentHealth = maxHealth;

    }
    private void Start()
    {
        healthBar.maxValue = maxHealth;
        healthBar.value = maxHealth;
        playerTransform = PlayerMove.Instance.transform;
    }

    void Update()
    {

        timeSinceLastUpdate += Time.deltaTime;
        MoveToPlayer();
    }
    private void MoveToPlayer()
    {
        float distanceToPlayer = Vector3.Distance(transform.position, playerTransform.position);

        if (distanceToPlayer <= attackRange)
        {
            navMesh.isStopped = true;
            anim.SetTrigger("isAttack");
        }
        else
        {
            if (timeSinceLastUpdate >= setTime)
            {
                navMesh.isStopped = false;
                navMesh.SetDestination(playerTransform.position);
                anim.SetTrigger("isRun");
                timeSinceLastUpdate = 0f;
            }
        }
    }
    public void TakeDamage(int damage)
    {
        currentHealth -= damage;
        healthBar.value = currentHealth;
        if(currentHealth <= 0)
        {
            EnemyManager.Instance.ReturnEnemy(gameObject);
        }
        
    }
  
}





   