using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour
{
    public int maxHealthPlayer = 100;
    public int currentHealthPlayer = 0;

    public Slider healthBar;
    public GameObject GameOverMenu;

    private void Awake()
    {
        currentHealthPlayer = maxHealthPlayer;
        
    }
    private void Update()
    {
        healthBar.maxValue = maxHealthPlayer;
    }
    private void TakeDamage(int damage)
    {
        currentHealthPlayer -= damage;
        healthBar.value = currentHealthPlayer;
        if(currentHealthPlayer <= 0)
        {
            GameOverMenu.SetActive(true);
            Time.timeScale = 0;
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("ZombieAttack"))
        {
            TakeDamage(5);
        }
    }
}
