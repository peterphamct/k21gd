﻿using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Animations;
using UnityEngine.Animations.Rigging;

public class PlayerMove : MonoBehaviour
{
    public GameObject AimingObject;
    public Transform shootStart;
    public CinemachineFreeLook AimingCam;
    public CinemachineFreeLook NormalCam;
    public Rig RigWeight;
    Animator ani;
    public CharacterController Ctrler;
    public Transform CameraTranform;
    public float Tourque = 0.5f;
    public float CurrentSpeed = 0;
    public float MaxSpeed = 9;
    public float WalkSpeed = 1.5f;
    bool aiming = false;
    bool shooting = false;
    BulletPool bulletPool = null;
    public static PlayerMove Instance { get; set; }

    private void Awake()
    {
        Instance = this;
        ani = GetComponent<Animator>();
    }
    private void Update()
    {
       
         shooting = Input.GetMouseButton(0);
        if (Input.GetMouseButtonDown(1))
        {
            aiming = !aiming;

            if (aiming)
            {
                CurrentSpeed = 0;
                ani.SetFloat("Speed", CurrentSpeed);
                AimingCam.m_XAxis = NormalCam.m_XAxis;
                AimingCam.m_YAxis = NormalCam.m_YAxis;
            }
            else
            {
                NormalCam.m_YAxis = AimingCam.m_YAxis;
                NormalCam.m_XAxis = AimingCam.m_XAxis;

            }
            AimingCam.gameObject.SetActive(aiming);
            NormalCam.gameObject.SetActive(!aiming);
        }


        if (!aiming)
            UpdateMove();
        else
            CheckAiming();
        RigWeight.weight = aiming ? 1 : 0;

        ani.SetBool("Aiming", aiming);
        ani.SetBool("Shooting", shooting);

    }
    private void CheckAiming()
    {
        Ray ray = Camera.main.ScreenPointToRay(new Vector3(Screen.width / 2f, Screen.height / 2f, 0f));
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit))
        {
            if (hit.transform.gameObject.name == AimingObject.name||hit.transform.gameObject.layer== LayerMask.NameToLayer("Player"))
                return;
            // Lấy điểm hit
            Vector3 hitPoint = hit.point;
            if (AimingObject != null)
            {
                MoveObjectToPosition(hitPoint);
            }
        }
        else
        {
            Vector3 endPoint = ray.GetPoint(200f); 
            if (AimingObject != null)
            {
                MoveObjectToPosition(endPoint);
            }
        }
        if (aiming)
        {
            Quaternion newRotation = Quaternion.LookRotation(Vector3.Scale(CameraTranform.forward, new Vector3(1, 0, 1)));
            transform.rotation = newRotation;
        }
    }
    private void MoveObjectToPosition(Vector3 targetPosition)
    {
        Vector3 newPosition = new Vector3(targetPosition.x, targetPosition.y, targetPosition.z);
        AimingObject.transform.position = newPosition;
    }
    private void UpdateMove()
    {
        float horizontalInput = Input.GetAxis("Horizontal");
        float verticalInput = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(horizontalInput, 0f, verticalInput).normalized;

        Vector3 cameraForward = Vector3.Scale(CameraTranform.forward, new Vector3(1, 0, 1));
        ani.SetFloat("X", horizontalInput*CurrentSpeed);
        ani.SetFloat("Y", verticalInput * CurrentSpeed);

        if (movement.magnitude <= 0.01f)
        {
            CurrentSpeed = 0;
            ani.SetFloat("Speed", CurrentSpeed);
        }
        else
        {
            CurrentSpeed += Tourque * Time.deltaTime;
            if (Input.GetKey(KeyCode.LeftShift))
            {
                CurrentSpeed = CurrentSpeed > MaxSpeed ? MaxSpeed : CurrentSpeed;
            }
            else
            {
                CurrentSpeed = CurrentSpeed > WalkSpeed ? WalkSpeed : CurrentSpeed;
            }
            ani.SetFloat("Speed", CurrentSpeed);
            if (CurrentSpeed > 0)
            {
                Quaternion newRotation = Quaternion.LookRotation(cameraForward);
                transform.rotation = newRotation;
            }
        }

    }
    private void OnShoot()
    {
        if (bulletPool == null)
        {
            bulletPool = BulletPool.Instance;
        }else
            bulletPool.ShootBullet(shootStart.position, AimingObject.transform.position);
    }
}
