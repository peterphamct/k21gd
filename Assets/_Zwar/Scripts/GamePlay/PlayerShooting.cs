using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShooting : MonoBehaviour
{
    public Transform bulletSpawnPoint;
    public float fireRate = 0.1f;
    private float fireCooldown = 0f;

    private void Update()
    {
        fireCooldown -= Time.deltaTime;

        if (Input.GetKeyDown(KeyCode.Space) && fireCooldown <= 0f)
        {
            Shoot();          
        }
    }

    private void Shoot()
    {
        //GameObject bullet = BulletPool.Instance.ShootBullet(bulletSpawnPoint.position, bulletSpawnPoint.rotation);
        fireCooldown = fireRate;
    }
    
}
