using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    public static EnemyManager Instance { get; private set; }
    public GameObject EnemyPrefab;

    private Queue<GameObject> enemyQueue = new Queue<GameObject>();

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
            return;
        }

        EnemyPool();
    }

    private void EnemyPool()
    {
        for (int i = 0; i < 10; i++)
        {
            CreateEnemy();
        }
    }

    private GameObject CreateEnemy()
    {
        GameObject newEnemy = Instantiate(EnemyPrefab);
        newEnemy.SetActive(false);
        enemyQueue.Enqueue(newEnemy);
        return newEnemy;
    }

    public GameObject SpawnEnemy(Vector3 position, Quaternion rotation)
    {
        if (enemyQueue.Count > 0)
        {
            GameObject enemy = enemyQueue.Dequeue();
            enemy.transform.position = position;
            enemy.transform.rotation = rotation;
            enemy.SetActive(true);
            return enemy;
        }
        return CreateEnemy();
    }

    public void ReturnEnemy(GameObject enemy)
    {
        enemy.SetActive(false);
        enemyQueue.Enqueue(enemy);
    }

}
