﻿using UnityEngine;

public class CarController : MonoBehaviour
{
    public WheelCollider FLWheel;
    public WheelCollider FRWheel;
    public WheelCollider BLWheel;
    public WheelCollider BRWheel;
    public Transform FLWheelTrans;
    public Transform FRWheelTrans;
    public Transform BLWheelTrans;
    public Transform BRWheelTrans;

    public float maxMotorTorque = 500f;
    public float maxSteerAngle = 30f;
    public float brakeTorque = 3000f;

    private float motorInput;
    private float steerInput;
    private bool isBraking;

    private void Update()
    {
        // Nhận dữ liệu từ bàn phím hoặc thiết bị người chơi
        motorInput = Input.GetAxis("Vertical");
        steerInput = Input.GetAxis("Horizontal");
        isBraking = Input.GetKey(KeyCode.Space);
    }

    private void FixedUpdate()
    {
        ApplyMotorTorque();
        ApplySteering();
        ApplyBrakes();

        UpdateWheelTransforms(FRWheel, FRWheelTrans);
        UpdateWheelTransforms(FLWheel, FLWheelTrans);
        UpdateWheelTransforms(BLWheel, BLWheelTrans);       
        UpdateWheelTransforms(BRWheel, BRWheelTrans);
        // UpdateWheelTransforms();
    }

    private void ApplyMotorTorque()
    {
        BRWheel.motorTorque = maxMotorTorque * motorInput;
        BLWheel.motorTorque = maxMotorTorque * motorInput;
        FLWheel.motorTorque = maxMotorTorque * motorInput;
        FRWheel.motorTorque = maxMotorTorque * motorInput;

    }

    private void ApplySteering()
    {
        FRWheel.steerAngle = maxSteerAngle * steerInput;
        FLWheel.steerAngle = maxSteerAngle * steerInput;
    }

    private void ApplyBrakes()
    {
       
            if (isBraking)
            {
                BRWheel.brakeTorque = brakeTorque;
                BLWheel.brakeTorque = brakeTorque;
                FLWheel.brakeTorque = brakeTorque;
                FRWheel.brakeTorque = brakeTorque;
            }
            else
            {
                BRWheel.brakeTorque = 0;
                BLWheel.brakeTorque = 0;
                FLWheel.brakeTorque = 0;
                FRWheel.brakeTorque = 0;
            }
        
    }

    private void UpdateWheelTransforms(WheelCollider wheelCollider,Transform wheelTransform)
    {
        Vector3 wheelPosition;
        Quaternion wheelRotation;
        wheelCollider.GetWorldPose(out wheelPosition, out wheelRotation);
        wheelTransform.position = wheelPosition;
        wheelTransform.rotation = wheelRotation;
    }
}
