using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public Transform spawnPoint;
    private float spawnTime = 2f;
    private float spawnTimeCooldown = 0f;

    private int maxEnemy = 10;
    private int minEnemy = 0;


    void Update()
    {
        spawnTimeCooldown -= Time.deltaTime;
        if (spawnTimeCooldown <= 0f)
        {
            SpawnEnemy();
            spawnTimeCooldown = spawnTime;
        }
    }

    private void SpawnEnemy()
    {
        if (minEnemy >= maxEnemy)
        {
            return;
        }
        EnemyManager.Instance.SpawnEnemy(spawnPoint.position, spawnPoint.rotation);
        minEnemy++;
    }
}
