﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField] private float bulletSpeed = 20f;
    [SerializeField] private float delayTriggerTime = 1;
    [SerializeField] private ParticleSystem TrigerFX;
    Rigidbody rb;
    [SerializeField] private float maxBulletDistance = 20f;
    float currentSpeed = 0;
    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }
    private void OnEnable()
    {
        StartCoroutine(CheckDistance());
        currentSpeed = bulletSpeed;
    }
    private void OnDisable()
    {
        StopAllCoroutines();
    }
    private void Update()
    {
        Move();

    }
    private void Move()
    {
        rb.velocity = transform.forward * currentSpeed;
    }
    private IEnumerator CheckDistance()
    {
        float traveledDistance = 0f;
        while(true)
        {
            traveledDistance += bulletSpeed * Time.deltaTime;
            if(traveledDistance >= maxBulletDistance)
            {
                BulletPool.Instance.ReturnBullet(gameObject);
                yield break;
            }
            yield return null;
        }
    }
    private void OnTriggerEnter(Collider other)
    {

        StartCoroutine(BulletTriger());
       // BulletPool.Instance.ReturnBullet(gameObject);
        //if (other.CompareTag("Enemy"))
        //{
        //    EnemyMove enemyHealth = other.GetComponent<EnemyMove>();
        //    enemyHealth.TakeDamage(10);
        //}
    }
    IEnumerator BulletTriger()
    {
        Debug.Log("trigger");
        // Đợi 1 giây
        currentSpeed = 0;
        TrigerFX.Play();
        yield return new WaitForSeconds(delayTriggerTime);
        BulletPool.Instance.ReturnBullet(gameObject);
    }

}

