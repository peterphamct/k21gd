using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

public class BulletPool : MonoBehaviour
{
    private static BulletPool instance;
    public static BulletPool Instance => instance;

    public GameObject bulletPrefab;
    public int initialBulletCount = 100;
    private Queue<GameObject> bulletQueue = new Queue<GameObject>();
    private Vector3 shootPos = Vector3.zero;

    private void Awake()
    {
        if (instance == null)
        {
           instance=this;
           InitializeObjectPool();
        }
        else
            Destroy(gameObject);

    }
    private void InitializeObjectPool()
    {
        for(int i = 0; i < initialBulletCount; i++)
        {
            CreateBullet();
        }
    }
    private GameObject CreateBullet()
    {
        GameObject newBullet = Instantiate(bulletPrefab);
        newBullet.SetActive(false);
        bulletQueue.Enqueue(newBullet);
        return newBullet;
    }
    public GameObject ShootBullet(Vector3 position,Vector3 target)
    {
        shootPos = position;
        if(bulletQueue.Count > 0)
        {
            GameObject bullet = bulletQueue.Dequeue();
            bullet.transform.position = position;
            bullet.transform.rotation = Quaternion.LookRotation(target-position);
            bullet.SetActive(true);
            return bullet;
        }
        else return CreateBullet();
    }
    public void ReturnBullet(GameObject bullet)
    {
        bullet.SetActive(false);
        bullet.transform.position = shootPos;
        bulletQueue.Enqueue(bullet);
    }
  

}
