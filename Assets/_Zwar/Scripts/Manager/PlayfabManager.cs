﻿using PlayFab;
using PlayFab.ClientModels;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayfabManager : MonoBehaviour
{
    public static PlayfabManager Instance;
    private string _playfabID;
    public string PlayFabID => _playfabID;
   
    public bool loginSucces;

    private void Awake()
    {

        if (Instance == null)
            Instance = this;
    }
    public void SetPlayFabID(string value)
    {
        _playfabID = value;
    }
    public void LoginWithCustomID(string IDLogin, Action<bool, PlayFabError,LoginResult> callback = null)
    {
        LoginWithCustomIDRequest request = new LoginWithCustomIDRequest()
        {
            CreateAccount = true,
            CustomId = IDLogin
        };

        PlayFabClientAPI.LoginWithCustomID(request, (result) =>
        {
            _playfabID = result.PlayFabId;
            Debug.Log("Got PlayFabID: " + _playfabID);

            if (result.NewlyCreated)
            {
                //   Debug.Log("(new account)");
            }
            else
            {
                //    Debug.Log("(existing account)");
            }

            callback?.Invoke(true, null,result);
        },
        (error) =>
        {
            callback?.Invoke(false, error,null);
        });
    }

    public void UpdatePlayerTitleData(string titleDataKey,string newData)
    {
        // Tạo một request để cập nhật title data
        UpdateUserDataRequest request = new UpdateUserDataRequest
        {
            Data = new Dictionary<string, string>
            {
                { titleDataKey, newData }
            }
        };

        // Gọi API để cập nhật title data cho người chơi đã đăng nhập
        PlayFabClientAPI.UpdateUserData(request, OnDataUpdated, OnDataUpdateError);
    }

    private void OnDataUpdated(UpdateUserDataResult result)
    {
        Debug.Log("Title data updated successfully.");
    }

    private void OnDataUpdateError(PlayFabError error)
    {
        Debug.LogError("Error updating title data: " + error.ErrorMessage);
    }

    public void GetTitleDataFromPlayFab(Action<GetUserDataResult> callBack)
    {
        var request = new GetUserDataRequest();
        PlayFabClientAPI.GetUserData(request, (result)=> { callBack?.Invoke(result); }, OnTitleDataFailure);
    }

   
    private void OnTitleDataFailure(PlayFabError error)
    {
        Debug.LogError("Title Data Request Failed: " + error.ErrorMessage);
    }

}
