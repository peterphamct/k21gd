using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class UIPopup : MonoBehaviour
{
    public UIPoupName name;
    public void Show()
    {
        OnShown();
    }
    public void Hide()
    {
        OnHiding();
    }
    protected virtual void OnShown()
    {
        Debug.Log("OnShown", this);
    }
    protected virtual void OnHiding()
    {
        Debug.Log("OnHide", this);
    }
}