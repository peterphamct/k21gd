using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public enum UIPoupName
{
    None = 0,
    Login = 1,
    Shop=2
}

public class UIManager : MonoBehaviour
{
    public static UIManager Instance;

    [SerializeField] private List<UIPopup> _listPopup = new List<UIPopup>();
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    public void ShowPopup(UIPoupName name)
    {

        UIPopup popupToShow = _listPopup.FirstOrDefault(item =>item.name==name);
        if (popupToShow != null)
        {
            popupToShow.gameObject.SetActive(true);
            popupToShow.Show();
        }
        else
        {
            Debug.LogError("No Popup with name : " + name.ToString());
        }
    }
    public void HidePopup(UIPoupName name)
    {

        UIPopup popupToHide = _listPopup.FirstOrDefault(item => item.name == name);
        if (popupToHide != null)
        {
            popupToHide.gameObject.SetActive(false);
            popupToHide.Hide();
        }
        else
        {
            Debug.LogError("No Popup with name : " + name.ToString());
        }
    }
    public void HideAllPopup()
    {
        foreach (var item in _listPopup)
        {
            item.gameObject.SetActive(false);
        }
    }

}
