using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class PreviewSystemManager : MonoBehaviour
{
    public static PreviewSystemManager Instance;
    List<GameObject> itemShowned = new List<GameObject>();
    public Transform ShownTrans;
    private void Awake()
    {
        Instance = this;
    }
    public void ShowItem(GameObject objToShown)
    {
        foreach(var item in itemShowned)
        {
            item.SetActive(false);
        }
        GameObject shownItem = itemShowned.FirstOrDefault(x => x.name == objToShown.name);
        if (shownItem== null)
        {
            shownItem=Instantiate(objToShown, ShownTrans);
            shownItem.transform.position = ShownTrans.position;
            shownItem.name = objToShown.name;
            itemShowned.Add(shownItem);
        }
        else
            shownItem.SetActive(true);
    }
}
